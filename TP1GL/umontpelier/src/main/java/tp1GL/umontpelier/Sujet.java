package tp1GL.umontpelier;


import java.io.FileWriter;
import java.io.IOException;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

public class Sujet {
	
	
	private int id;
	private String titre;
	
	
	public Sujet(int id, String titre) {
		super();
		this.id = id;
		this.titre = titre;
	}
	
	public Sujet()
	{
		
	}
	
	
	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}


	public String getTitre() {
		return titre;
	}


	public void setTitre(String titre) {
		this.titre = titre;
	}


	@SuppressWarnings("unchecked")
	public void createFile(JSONArray sujetList)
	{
		JSONObject sujetDetails = new JSONObject();
		sujetDetails.put("id", this.getId());
		sujetDetails.put("titre", this.getTitre());
		
		JSONObject groupe = new JSONObject();
		groupe.put("sujet", sujetDetails);
		

		sujetList.add(groupe);
		
		try {
			FileWriter file = new FileWriter("/home/e20210012331/Bureau/TPGenieLogiciel/TP1GL/sujet.json");
			file.write(sujetList.toJSONString());
			file.flush();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	
		
	}
	
	
	

}
