package tp1GL.umontpelier;


import java.io.FileWriter;
import java.io.IOException;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;


	public class Groupe {
		
		
		private int id;
		private String nom;
		
		public Groupe(int id, String nom) {
			
			this.id = id;
			this.nom = nom;
		}
		

		
		public int getId() {
			return id;
		}


		public void setId(int id) {
			this.id = id;
		}


		public String getNom() {
			return nom;
		}


		public void setNom(String nom) {
			this.nom = nom;
		}
		
		// La serialization
		@SuppressWarnings("unchecked")
		public void createFile(JSONArray grpList)
		{
			JSONObject grpDetails = new JSONObject();
			grpDetails.put("id", this.getId());
			grpDetails.put("nom", this.getNom());
//			
//			JSONObject groupe = new JSONObject();
//			groupe.put("groupe", grpDetails);
			

			grpList.add(grpDetails);
			
			try {
				FileWriter file = new FileWriter("/home/e20210012331/Bureau/TPGenieLogiciel/TP1GL/groupe.json");
				file.write(grpList.toJSONString());
				file.flush();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		
			
		}
		

	}


