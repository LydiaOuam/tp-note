package tp1GL.umontpelier;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class Main {


	@SuppressWarnings("unchecked")
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		
		Groupe grp = new Groupe(1,"nom1");
	
		Groupe grp1 = new Groupe(2,"nom2");
		Groupe grp2 = new Groupe(3,"nom3" );
		
		Sujet s = new Sujet(1,"s1");
		Sujet s2 = new Sujet(2,"s2");
		//Sujet
		JSONArray suj = new JSONArray();
		s.createFile(suj);
		s2.createFile(suj);
		//Groupe
		JSONArray ja = new JSONArray();
		grp.createFile(ja);
		grp1.createFile(ja);
		grp2.createFile(ja);
		
		//La deserialization
		JSONArray array = readFile(new File("/home/e20210012331/Bureau/TPGenieLogiciel/TP1GL/groupe.json"));
//		JSONArray array2 = readFile(new File("/home/e20210012331/Bureau/TPGenieLogiciel/TP1GL/sujet.json"));

		
//		array2.forEach(item -> {
//			JSONObject object = (JSONObject) item;
//			parseSujetObject(object);
//			JSONObject j = (JSONObject) object.get("sujet");
//			int i = (int)(long) j.get("id");
//			String n = (String) j.get("titre");
//			System.out.println(i);
//			System.out.println(n);
//		});
		//	---------------------------------------
		
			
			array.forEach(item -> {
				JSONObject object = (JSONObject) item;
				parseGroupeObject(object);
//				JSONObject j = (JSONObject) object.get("groupe");
//				int i = (int)(long) j.get("id");
//				String n = (String) j.get("nom");
//				System.out.println(i);
//				System.out.println(n);
			});
			
		
		

	}
	
	//Des methodes utilise lors de la deserialization
	
	public static void parseGroupeObject(JSONObject groupe)
	{
//		JSONObject groupeObject = (JSONObject) groupe.get("groupe");
		
		
		int id = (int) (long) groupe.get("id");
		System.out.println(id);
		
		String nom = (String) groupe.get("nom");
		System.out.println(nom);
	}

	public static void parseSujetObject(JSONObject sujet)
	{
		JSONObject sujetObject = (JSONObject) sujet.get("sujet");
		
		
		int id = (int) (long) sujetObject.get("id");
		System.out.println(id);
		
		String titre = (String) sujetObject.get("titre");
		System.out.println(titre);
	}


	
	public static JSONArray readFile(File file)
	{
	JSONParser parser = new JSONParser();
		
		try {
			FileReader reader = new FileReader(file);
			Object obj = parser.parse(reader);
			
			JSONArray array = (JSONArray)obj;
			return array;
//			System.out.println(array);
			
			
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	return null;
	}



}
